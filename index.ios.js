'use strict';

import Login from './app/ios/login';
import reducers from './app/reducers';

import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import React, { Component } from 'react';

import {
  AppRegistry,
  NavigatorIOS,
} from 'react-native';

const middleware = applyMiddleware(thunkMiddleware, createLogger)
const store = createStore(reducers, middleware);

export default class guideable extends Component {
  render() {
    return (
      <Provider store={store}>
        <NavigatorIOS 
          navigationBarHidden={true} 
          ref="nav" 
          initialRoute={{title: "Guideable", component: Login}} 
          style={{flex: 1}}/>
      </Provider>
    );
  }
}

AppRegistry.registerComponent('guideable', () => guideable);
