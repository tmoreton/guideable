// Initialize Firebase
import * as firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyDLWC0K1Kgr73peDQa4wFCCAUqqI10wV9A",
  authDomain: "guidable-91ff7.firebaseapp.com",
  databaseURL: "https://guidable-91ff7.firebaseio.com",
  projectId: "guidable-91ff7",
  storageBucket: "guidable-91ff7.appspot.com",
  messagingSenderId: "585200007820"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

module.exports = firebaseApp