export default reducers = (state = {
	user: {
		id: '',
		photo: '',
		name: '',
		email: ''
	},
	guide: {
		title: '',
		city: { name: '', lat: 0, lng: 0},
		places: { 0: { name: '', lat: 0, lng: 0, category: '', address: ''}},
		key: 0,
		description: '',
		image: '',
		ownerId: '',
	},
	}, action) => {
		switch (action.type) {
			case 'LOGIN_SUCCESS': {
				return {...state,
					user: action.payload,
					guide: { ...state.guide, ownerId : action.payload.id }
				}
			}
			case 'CLEAR_POST':
				return { ...state, 
					guide: {
						title: '',
						city: { name: '', lat: 0, lng: 0},
						places: { 0: { name: '', lat: 0, lng: 0, category: '', address: ''}},
						key: 0,
						description: '',
						image: '',
						ownerId: action.ownerId,
					} 
			}	
			case 'ADD_POST_TITLE': 			
				return { ...state, guide: { ...state.guide, title : action.payload } }
			case 'POST': 			
				return { ...state, guide: action.guide }
			case 'GO_TO_POST': 			
				return { ...state, guide: action.guide }
			case 'ADD_KEY': 			
				return { ...state, guide: { ...state.guide, key : action.key } }
			case 'REMOVE_PLACE': 			
				return { ...state, guide: { ...state.guide, places : { ...state.guide.places, [action.key]: '' } } }
			case 'ADD_PLACES': 			
				return { ...state, guide: { ...state.guide, places : { ...state.guide.places, [action.key]: action.place } } }
			case 'ADD_POST_IMAGE': 			
				return { ...state, guide: { ...state.guide, image : action.payload } }
			case 'UPDATE_POST_CITY': 			
				return { ...state, guide: { ...state.guide, city : { ...state.guide.city, name: action.payload } } }
			case 'UPDATE_POST_LOCATION': 			
				return { ...state, guide: { ...state.guide, location : { ...state.guide.location, name: action.payload } } }
			case 'ADD_POST_CITY': 			
				return { ...state, guide: { ...state.guide, city : { ...state.guide.city, name: action.name, lat: action.lat, lng: action.lng} } }
			case 'ADD_POST_DESCRIPTION': 			
				return { ...state, guide: { ...state.guide, description : action.payload } }	
			case 'ADD_POST_LOCATION': 			
				return { ...state, guide: { ...state.guide, location : { ...state.guide.location, name: action.name, lat: action.lat, lng: action.lng, category: action.category, address: action.address} } }
			case 'CLEAR_LOCATION': 			
				return { ...state, guide: { ...state.guide, location : { ...state.guide.location, name: action.clear, lat: action.clear, lng: action.clear, category: action.clear, address: action.clear} } }
		}
	  return state;
} 