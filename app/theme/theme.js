const React = require('react-native')
const {StyleSheet} = React
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;
const constants = {
  font: "HelveticaNeue-Thin",
    border: '#dbdbdb',
  lightGreen: "#56bd7a",
  color:"#61BBB5"
};

var styles = StyleSheet.create({
  container: {
    flex: 1
  },
  full: {
    width: deviceWidth
  },
  flex: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  left: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: 75,
  },
  right: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  btn: {
    backgroundColor: '#fff',
    borderColor: constants.border,
    borderWidth: 1,
    padding: 10,
    borderRadius: 3,
    minWidth: 150, 
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: constants.font,
    margin: 5,
    fontSize: 18
  },
  postBtn: {
    backgroundColor: '#61BBB5',
    color: '#fff',
    padding: 10,
    borderRadius: 3,
    minWidth: 150, 
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: constants.font,
    margin: 5,
    fontSize: 18
  },
  bottomRow: {
    height:60, 
    borderTopWidth:1, 
    borderTopColor: '#ddd', 
    flexDirection:'row', 
    justifyContent: 'space-between', 
    alignItems: 'center',
    padding: 20,
    backgroundColor: "#61BBB5",
    position: 'absolute',
    bottom: 0,
    width: deviceWidth
  },
  bottomRight: {
    alignItems: 'flex-end',
  },
  bottomLeft: {
    alignItems: 'flex-start',
  },
  white: {
    color: '#fff'
  },
  listview: {
    flex: 1,
  },
  input: {  
    height: 45,
    width: deviceWidth,
    fontSize: 24,
    textAlign: 'center',
    fontFamily: constants.font,
  },
  description: {  
    height: 200,
    width: 300,
    fontSize: 24,
    textAlign: 'center',
    fontFamily: constants.font,
  },
  text: {
    fontFamily: constants.font,
    fontSize: 18,
    textAlign: 'center',
  },
  cityText: {
    fontFamily: constants.font,
    fontSize: 26,
    textAlign: 'center',
    paddingTop: 50
  },
  headerTitle: {
    fontFamily: constants.font,
    fontSize: 25,
    },
  logoText: {
    fontFamily: constants.font,
    color: '#fff',
    paddingBottom: 30,
    width: deviceWidth,
    fontSize: 45,
    textAlign: 'center'
  },
  postTitle: {
    fontFamily: constants.font,
    fontSize: 30,
    paddingBottom: 5,
    textAlign: 'center'
  },
  postCity: {
    fontFamily: constants.font,
    fontSize: 18,
    paddingBottom: 5,
    textAlign: 'center',
  },
  postDescription: {
    fontFamily: constants.font,
    fontSize: 12,
    paddingBottom: 15,
    textAlign: 'center',
  },
  locationDescription: {
    fontFamily: constants.font,
    fontSize: 12,
    textAlign: 'center',
  },
  postRemove: {
    fontFamily: constants.font,
    fontSize: 12,
    paddingBottom: 10,
    textAlign: 'center',
    color: 'red',
    marginTop: -10
  },
  line: {
    borderColor: constants.border,
    borderWidth: 0.5,
    height: 1,
    alignSelf: 'stretch',
  },
  header: {
    backgroundColor: '#fff',
  },
  icon: {
    borderWidth: 1, 
    borderRadius: 12, 
    padding: 15,
  },
  navbar: {
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
    height: 55,
    flexDirection: 'row',
    marginTop: 15,
  },
  navText:{
    fontFamily: constants.font,
    fontSize: 16
  },
  mapContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  loginScreen: {
    flex: 1,
    backgroundColor: constants.color,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    padding: 0,
    margin: 0
  },
  container: {
    flex: 1, 
    backgroundColor: '#222', 
    opacity: 0.6, 
  }, 
  articlePreview: {
    flex: 1, 
    flexDirection: 'column',
    width: deviceWidth, 
    height: (deviceWidth*.5)
  }, 
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    borderWidth: 1,
    borderColor: '#fff',
    margin: 10,
    padding: 10,
    justifyContent: 'center',
  },
  previewName: {
    fontFamily: constants.font,
    fontSize: 30,
    color: '#fff', 
    textAlign: 'center', 
  },
  previewCity: {
    fontFamily: constants.font,
    fontSize: 21,
    color: '#fff', 
    textAlign: 'center',
    paddingTop: 5,
    paddingBottom: 10 
  }, 
})

module.exports = styles
module.exports.constants = constants;
