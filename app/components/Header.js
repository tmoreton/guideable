'use strict';
import React, {Component} from 'react';
import ReactNative from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = require('../theme/theme')
import { connect } from 'react-redux';

const { 
  Text, 
  View, 
  TouchableOpacity,
  Image
} = ReactNative;

class Header extends Component {
  userProfile(){
    if(this.props.right != null){
      return (
        <View style={styles.right}>
          <Image source={{uri: this.props.user.photo}} style={{width: 30, height: 30, borderRadius: 15 }}/>
          <Text style={{ fontSize: 10 }}>{this.props.user.name}</Text>
        </View>
      )
    }
  }

  render() {
    return (
      <View style={styles.header}>
        <View style={styles.navbar}>
          <TouchableOpacity onPress={this.props.left} style={styles.left} >
            <Icon name={this.props.icon} size={25} />
            <Text style={{ fontSize: 10 }}>{this.props.leftText}</Text>
          </TouchableOpacity>
          <Text style={styles.headerTitle}>{this.props.title}</Text>
          <TouchableOpacity onPress={this.props.right} style={{ width: 75 }} >
            {this.userProfile()}
          </TouchableOpacity>
        </View>
        <View style={styles.line} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

export default connect(mapStateToProps)(Header);