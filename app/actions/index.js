import firebase from '../config/firebase';
import PostReview from '../ios/postReview';
import App from '../ios/app';
var ImagePicker = require('react-native-image-picker');
import RNFetchBlob from 'react-native-fetch-blob';
import ImageResizer from 'react-native-image-resizer';
const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs


export function login(result){
	var user = {}
	user.id = result.id,
	user.name = result.name
	user.photo = 'https://graph.facebook.com/' +result.id+ '/picture'
	return function(dispatch) {
		dispatch({
			type: 'LOGIN_SUCCESS', 
			payload: user
		});
	}
}

export function postGuide(guide){
	return function(dispatch){
		firebase.database().ref('guides').push(guide.state.guide)
		dispatch({ type: 'CLEAR_POST', guide: '' })
		guide.props.navigator.push({ component: App })
	}
}

export function goToPost(guide){
	return function(dispatch){
		dispatch({ type: 'GO_TO_POST', guide: guide })
	}
}

export function clearPost(ownerId){
	return {
		type: 'CLEAR_POST',
		payload: '',
		ownerId: ownerId
	}
}

export function addPostTitle(title){
	return {
		type: 'ADD_POST_TITLE',
		payload: title
	}
}

export function updatePostCity(city){
	return {
		type: 'UPDATE_POST_CITY',
		payload: city
	}
}

export function updatePostLocation(location){
	return {
		type: 'UPDATE_POST_LOCATION',
		payload: location
	}
}

export function addPostCity(city){
	return function(dispatch) {
		fetch("https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + city + "&key=AIzaSyChfLOIW6EJdhdEEKpRCxuQEtXRZs8IQR0", {method: "GET"})
		.then((response) => response.json())
		.then((responseData) => {
			if(responseData.results[0] != null ){
				dispatch({ type: 'ADD_POST_CITY', 
					name: responseData.results[0].name,
					lat: responseData.results[0].geometry.location.lat,
					lng: responseData.results[0].geometry.location.lng})
			}
		})
    .catch((error) => {
      alert(error);
    });
	}
}

export function addPostDescription(description){
	return {
		type: 'ADD_POST_DESCRIPTION',
		payload: description
	}
}

export function removePlace(key){
	return function(dispatch) {
		dispatch({ type: 'REMOVE_PLACE', key: key})
	}
}

export function addPlaces(place, key){
	return function(dispatch) {
		var newKey = key + 1
		dispatch({ type: 'ADD_PLACES', place: place, key: key})
		dispatch({ type: 'CLEAR_LOCATION', clear: ''})
		dispatch({ type: 'ADD_KEY', key: newKey})
	}
}

export function addPostLocation(location, city){
	return function(dispatch) {
		var url;
		if(city != ''){
			url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + location.replace(" ", "+") + "+in+" + city.name + "&key=AIzaSyChfLOIW6EJdhdEEKpRCxuQEtXRZs8IQR0"
		} else {
			url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + location.replace(" ", "+") + "&key=AIzaSyChfLOIW6EJdhdEEKpRCxuQEtXRZs8IQR0"
		}
		fetch(url, {method: "GET"})
		.then((response) => response.json())
		.then((responseData) => {
				if(responseData.results[0] != null ){
					dispatch({ type: 'ADD_POST_LOCATION', 
						name: responseData.results[0].name,
						lat: responseData.results[0].geometry.location.lat,
						lng: responseData.results[0].geometry.location.lng,
						category: responseData.results[0].types[0],
						address: responseData.results[0].formatted_address})
				}
		})
    .catch((error) => {
      alert(error);
    });
	}
}

export function addPostImage(){
	return function(dispatch){
		window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
		window.Blob = Blob
		ImagePicker.showImagePicker({}, (response) => {
		  if (!response.didCancel) {
		    const source = {uri: response.uri.replace('file://', ''), isStatic: true};

				ImageResizer.createResizedImage(source.uri, 500, 500, 'JPEG', 60).then((resizedImageUri) => {
					uploadImage(resizedImageUri)
					  .then(url => dispatch({ type: 'ADD_POST_IMAGE', payload: url}))
					  .catch(error => console.log(error))
				}).catch((err) => {
					console.log(err)
				});

		  }
		});
	}
}

const uploadImage = (uri, mime = 'application/octet-stream') => {
  return new Promise((resolve, reject) => {
    const uploadUri = uri
    const sessionId = new Date().getTime()
    let uploadBlob = null
    const imageRef = firebase.storage().ref('images').child(`${sessionId}`)

    fs.readFile(uploadUri, 'base64')
      .then((data) => {
        return Blob.build(data, { type: `${mime};BASE64` })
      })
      .then((blob) => {
        uploadBlob = blob
        return imageRef.put(blob, { contentType: mime })
      })
      .then(() => {
        uploadBlob.close()
        return imageRef.getDownloadURL()
      })
      .then((url) => {
        resolve(url)
      })
      .catch((error) => {
        reject(error)
    })
  })
}