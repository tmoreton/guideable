'use strict';
import React, {Component} from 'react';
import firebase from '../config/firebase';
import ReactNative from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = require('../theme/theme')
import Header from '../components/Header';
import { postGuide, clearPost } from '../actions'
import { connect } from 'react-redux';
import App from './app';
import Map from './map';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

const { 
  Text, 
  View, 
  TouchableOpacity,
  Image,
  TextInput,
  AlertIOS,
  ScrollView,
} = ReactNative;

class PostReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      guide: ''
    }
  }

  componentWillMount() {
    var newGuide = this.props.guide
    newGuide.user = this.props.user
    this.setState({
      guide: newGuide
    })
  }

  onHome(){
    this.props.navigator.push({ component: App });
  }

  onBack(){
    this.props.navigator.pop();
  }

  onSubmit(){
    this.props.dispatch(postGuide(this))
  }

  viewMap(){
    this.props.navigator.push({ component: Map });
  }

  render() {
    return (
        <View style={{flex: 1}}>
          <Header title="Review" left={this.onHome.bind(this)} icon="home"/>
          <ScrollView >
            <Image source={{uri: this.props.guide.image}}  style={{ marginTop: -25, width: deviceWidth, height: (deviceWidth*.5)}}/>
            <Text style={styles.postTitle}>{this.props.guide.title}</Text>
            <Text style={styles.postCity}><Icon name="globe" size={20} /> {this.props.guide.city.name}</Text>
            <Text style={styles.postDescription}>{this.props.guide.description}</Text>
            {Object.keys(this.props.guide.places).map((key) => {
              if(this.props.guide.places[key] != '' && this.props.guide.places[key].name != ''){
                var icon;
                switch(this.props.guide.places[key].category) {
                  case "cafe":
                    icon = "coffee"
                    break;
                  case "bakery":
                    icon = "coffee"
                    break;
                  case "restaurant":
                    icon = "cutlery"
                    break;
                  case "bar":
                    icon = "glass"
                    break;
                  default:
                    icon = "map-marker"
                }
                return (
                  <View style={styles.flex}>
                    <Text style={styles.text}><Icon name={icon} size={18} /> {this.props.guide.places[key].name}</Text>
                    <Text style={styles.postDescription}>{this.props.guide.places[key].address.split(",")[0]}</Text>
                  </View>
                )
              }
            })}
            <TouchableOpacity style={ styles.btn } onPress={this.viewMap.bind(this)} >
              <Text style={styles.text}><Icon name="map" size={20} /> View Map</Text>
            </TouchableOpacity>
          </ScrollView>
          <View style={ styles.bottomRow } >
            <TouchableOpacity onPress={this.onBack.bind(this)} style={ styles.bottomLeft }>
              <Text style={[styles.white, styles.text]}><Icon style={ styles.white } name="arrow-left" size={25} /> Back</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onSubmit.bind(this)} style={ styles.bottomRight }>
              <Text style={[styles.white, styles.text]}>Submit <Icon style={ styles.white } name="arrow-right" size={25} /></Text>
            </TouchableOpacity>
          </View>
        </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    guide: state.guide,
    user: state.user
  };
}

export default connect(mapStateToProps)(PostReview);