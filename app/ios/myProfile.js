'use strict';
const styles = require('../theme/theme')
import Header from '../components/Header';
import Post from './postTitle';
import Guide from './guide';
import App from './app';
import MyGuides from './myGuides';
import MyMap from './myMap';
import logout from './login';
import MySaves from './mySaves';
import Icon from 'react-native-vector-icons/FontAwesome';
import firebase from '../config/firebase';
import { connect } from 'react-redux';
import React, { Component } from 'react';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';

class MyProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      guides: []
    }
  }

  post(){
    this.props.navigator.push({ component: Post });
  }

  logout(){
    this.props.navigator.push({ component: logout });
  }

  onHome(){
  	this.props.navigator.push({ component: App });
  }

  myGuides(){
    this.props.navigator.push({ component: MyGuides });
  }

  myMap(){
    this.props.navigator.push({ component: MyMap });
  }

  mySaves(){
    this.props.navigator.push({ component: MySaves });
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <Header title="My Profile" left={this.onHome.bind(this)} icon="home"/>
        <Image source={{uri: 'https://firebasestorage.googleapis.com/v0/b/guidable-91ff7.appspot.com/o/default-map.jpg?alt=media&token=082521e8-9243-4354-b401-45ace62a8846'}}  style={{  width: deviceWidth, height: (deviceWidth*.5)}}/>
        <View style={{ alignItems: 'center', padding: 15,}}>
          <Image source={{uri: this.props.user.photo}} style={{width: 30, height: 30, borderRadius: 15 }}/>
          <Text style={{ fontSize: 10 }}>{this.props.user.name}</Text>
            <TouchableOpacity onPress={this.myGuides.bind(this)} style={styles.btn}>
              <Text style={styles.text}><Icon name="map" size={15} /> My Guides</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.myMap.bind(this)} style={styles.btn}>
              <Text style={styles.text}><Icon name="globe" size={15} /> Places I've Been</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.mySaves.bind(this)} style={styles.btn}>
              <Text style={styles.text}>Saved Guides</Text>
            </TouchableOpacity>
            <Text>Version "2.7.18"</Text>
            <TouchableOpacity onPress={this.logout.bind(this)}>
              <Text style={styles.text}>Logout</Text>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    guide: state.guide,
    user: state.user
  };
}

export default connect(mapStateToProps)(MyProfile);