'use strict';
import React, {Component} from 'react';
import firebase from '../config/firebase';
import ReactNative from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = require('../theme/theme')
import Header from '../components/Header';
import { connect } from 'react-redux';
import MapView from 'react-native-maps';
import App from './app';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

const { 
  Text, 
  View, 
  TouchableOpacity,
  Image,
  TextInput,
  AlertIOS,
  Button,
} = ReactNative;

class PostReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      guides: []
    }
  }

  onHome(){
    this.props.navigator.push({ component: App });
  }

  onBack(){
    this.props.navigator.pop();
  }

  componentWillMount(){
    this.getGuides()
  }

  getGuides() {
    var guides = firebase.database().ref('guides');
    guides.orderByChild("ownerId").equalTo(this.props.user.id).once("value", (snap) => {
      var items = [];
      snap.forEach((child) => {
        var item = child.val().city;
        items.push(item);    
      });
      console.log(items)
      this.setState({ guides: items });
    });
  }

  render() {
    return (
      <View style={styles.mapContainer}>
        <MapView
          style={styles.map}
        >
          {Object.keys(this.state.guides).map((key) => {
            return(
              <MapView.Marker
                coordinate={{
                  latitude: this.state.guides[key].lat,
                  longitude: this.state.guides[key].lng
                }}
                title={this.state.guides[key].name}
              />
            )
          })}
        </MapView>
        <TouchableOpacity style={ styles.btn } onPress={this.onBack.bind(this)} >
          <Text style={styles.text}><Icon name="arrow-left" size={25} /> Back</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    guide: state.guide,
    user: state.user
  };
}

export default connect(mapStateToProps)(PostReview);         

