'use strict';
import React, {Component} from 'react';
import ReactNative from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = require('../theme/theme')
import Header from '../components/Header';
import { addPostLocation, updatePostLocation, addPlaces, removePlace } from '../actions'
import { connect } from 'react-redux';
import App from './app';
import PostImage from './postImage';
import * as Progress from 'react-native-progress';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;

const { 
  Text, 
  View, 
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  Keyboard,
  ActivityIndicator,
  AlertIOS
} = ReactNative;

class PostPlace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: { name: '', lat: '', lng: '', category: '', address: ''},
      name: '',
      animating: false
    }
  }

  onHome(){
    this.props.navigator.push({ component: App });
  }

  onBack(){
    this.props.navigator.pop();
  }

  onNext(){
    if(this.props.guide.places[0].name != ''){
      this.props.navigator.push({ component: PostImage });
    } else {
      AlertIOS.alert("Add Places To Your Guide")
    }
  }

  findLocation(){
    this.setState({ animating: true })
    var url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + this.state.name.replace(" ", "+") + "+in+" + this.props.guide.city.name + "&key=AIzaSyChfLOIW6EJdhdEEKpRCxuQEtXRZs8IQR0";
    fetch(url, {method: "GET"})
    .then((response) => response.json())
    .then((responseData) => {
        if(responseData.results[0] != null ){
          this.setState({
            location:{
              name: responseData.results[0].name,
              lat: responseData.results[0].geometry.location.lat,
              lng: responseData.results[0].geometry.location.lng,
              category: responseData.results[0].types[0],
              address: responseData.results[0].formatted_address
            }
          })
        }
    })
    .catch((error) => {
      alert(error);
    });


    // this.props.dispatch(addPostLocation(this.state.location, this.props.guide.city))
    // this.setState({ 
    //   location: { name: '', lat: '', lng: '', category: '', address: ''},
    //   animating: true
    // })
  }

  addPlace(){
    this.setState({ 
      location: { name: '', lat: '', lng: '', category: '', address: ''},
      name: '',
      animating: false 
    })
    this.props.dispatch(addPlaces(this.state.location, this.props.guide.key))
  }

  remove(){
    this.self.props.dispatch(removePlace(this.key))
  }

  removeKeyboard(){
    Keyboard.dismiss()
  }

  renderLocation(){
    if(this.state.location.address != ''){
      return (
        <TouchableOpacity onPress={this.addPlace.bind(this)} style={styles.btn}>
          <Text style={ styles.text } >{ this.state.location.name } <Icon name="plus" size={20} color="#61BBB5" /></Text>
          <Text style={ styles.locationDescription } >{ this.state.location.address }</Text>
        </TouchableOpacity>
      )
    } else {
      return (
        <View>
          <ActivityIndicator animating={this.state.animating} size = "large" />
        </View>
      )
    }
  }

  render() {
    return (
        <View style={{flex: 1}}>
          <Header title="Places" left={this.onHome.bind(this)} icon="home"/>
          <Progress.Bar progress={0.8} color="#61BBB5" width={deviceWidth} />
          <View style={{ paddingTop: 100 }}>
            <Text style={styles.postTitle} onPress={this.removeKeyboard.bind(this)}>Add a New Place</Text>
            <View style={{flexDirection:'row'}}>
            <TextInput 
              placeholder="Find Places"
              ref="Location" 
              style={ styles.input }
              value={ this.state.name }
              onChangeText={(event) => this.setState({name: event })}/>
            </View>
            <View style={ styles.line }/>
            <TouchableOpacity onPress={this.findLocation.bind(this)} style={styles.postBtn}>
              <Text style={[styles.text, styles.white]}>Search <Icon name="search" size={20} /></Text>
            </TouchableOpacity>
          </View>
          { this.renderLocation() }
          <ScrollView style={{ marginBottom: 60 }} >
          {Object.keys(this.props.guide.places).map((key) => {
            if(this.props.guide.places[key] != '' && this.props.guide.places[key].name != ''){
              var icon;
              switch(this.props.guide.places[key].category) {
                case "cafe":
                  icon = "coffee"
                  break;
                case "bakery":
                  icon = "coffee"
                  break;
                case "restaurant":
                  icon = "cutlery"
                  break;
                case "bar":
                  icon = "glass"
                  break;
                default:
                  icon = "map-marker"
              }
              return (
                <View>
                  <Text style={styles.text}><Icon name={icon} size={18} /> {this.props.guide.places[key].name}</Text>
                  <Text style={styles.postDescription}>{this.props.guide.places[key].address}</Text>
                  <TouchableOpacity onPress={this.remove.bind({self: this, key: key})}>
                    <Text style={styles.postRemove}>Remove</Text>
                  </TouchableOpacity>
                </View> 
              )
            }
          })}
          </ScrollView>
          <View style={ styles.bottomRow } >
            <TouchableOpacity onPress={this.onBack.bind(this)} style={ styles.bottomLeft }>
              <Text style={[styles.white, styles.text]}><Icon style={ styles.white } name="arrow-left" size={25} /> Back</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onNext.bind(this)} style={ styles.bottomRight }>
              <Text style={[styles.white, styles.text]}>Add Photo <Icon style={ styles.white } name="arrow-right" size={25} /></Text>
            </TouchableOpacity>
          </View>
        </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    guide: state.guide,
    user: state.user
  };
}

export default connect(mapStateToProps)(PostPlace);