'use strict';
const styles = require('../theme/theme')
import Header from '../components/Header';
import Post from './postTitle';
import Guide from './guide';
import App from './app';
import Profile from './myProfile';
import Icon from 'react-native-vector-icons/FontAwesome';
import firebase from '../config/firebase';
import { signIn } from '../actions'
import { connect } from 'react-redux';
import React, { Component } from 'react';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';

class MyGuides extends Component {
  constructor(props) {
    super(props);
    this.state = {
      guides: []
    }
  }

  componentDidMount(){
    this.getGuides()
  }

  profile(){
    this.props.navigator.push({ component: Profile });
  }

  back(){
    this.props.navigator.pop();
  }

  home(){
    this.props.navigator.push({ component: App });
  }

  goToPost(){
    this.self.props.navigator.push({ 
      component: Guide,
      passProps: { guide: this.guide } 
    });
  }

  deletePost(){
    // this.self.props.dispatch(this.self.goToEditPost(this.guide))
    firebase.database().ref('guides/' + this.guide.firebaseId).remove();
    this.self.props.navigator.push({ component: Profile });
  }

  getGuides() {
    var guides = firebase.database().ref('guides');
    guides.orderByChild("ownerId").equalTo(this.props.user.id).once("value", (snap) => {
      var items = [];
      snap.forEach((child) => {
        var item = child.val();
        item.firebaseId = child.key
        items.push(item);    
      });
      items = items.reverse();
      this.setState({ guides: items });
    });
  }

  newPost(){
    this.props.navigator.push({ component: Post });
  }

  render() {
    if ( this.state.guides.length < 1 ){
      return(
        <View style={{flex: 1}}>
          <Header title="My Guides" left={this.back.bind(this)} icon="arrow-left"/>
          <Text style={ styles.text}>You Haven Not Made A Guide Yet :(</Text>
          <TouchableOpacity style={ styles.postBtn } onPress={ this.newPost.bind(this) } >
            <Text style={[ styles.text, styles.white ]}>Create One!</Text>
          </TouchableOpacity>
        </View>
      )
    } else {
      return (
        <View style={{flex: 1}}>
          <Header title="My Guides" left={this.back.bind(this)} icon="arrow-left"/>
          <ScrollView>
            <View>
            {Object.keys(this.state.guides).map((key) => {
              return (
                <TouchableHighlight >
                  <View>
                    <Image source={{uri: this.state.guides[key].image}} style={styles.articlePreview}>
                      <View style={styles.container}/>
                    </Image>
                    <TouchableOpacity style={styles.overlay} key={key} onPress={ this.goToPost.bind({self: this, guide: this.state.guides[key]}) }>
                      <Text style={styles.previewName}>{this.state.guides[key].title}</Text>
                      <Text style={styles.previewCity}><Icon name="globe" size={20} /> {this.state.guides[key].city.name}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={ this.deletePost.bind({self: this, guide: this.state.guides[key]}) } style={{backgroundColor: '#c90000', alignItems: 'center', justifyContent: 'center', width: deviceWidth, height: 35}} >
                      <Text style={{fontSize: 16, color: '#fff'}}>Delete</Text>
                    </TouchableOpacity>
                  </View>
                </TouchableHighlight>
              )
            })}
            </View>
          </ScrollView>
        </View>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    guide: state.guide,
    user: state.user,
  };
}

export default connect(mapStateToProps)(MyGuides);