'use strict';
import React, {Component} from 'react';
import ReactNative from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = require('../theme/theme')
import Header from '../components/Header';
import { addPostDescription } from '../actions'
import { connect } from 'react-redux';
import App from './app';
import Location from './postLocation';
import * as Progress from 'react-native-progress';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;

const { 
  Text, 
  View, 
  TouchableOpacity,
  Image,
  TextInput,
  Keyboard,
  AlertIOS
} = ReactNative;

class PostDescription extends Component {

  onHome(){
    this.props.navigator.push({ component: App });
  }

  onBack(){
    this.props.navigator.pop();
  }

  onNext(){
    if(this.props.guide.description != ''){
      this.props.navigator.push({ component: Location });
    } else {
      AlertIOS.alert("Add Short Description About Guide")
    }
  }

  removeKeyboard(){
    Keyboard.dismiss()
  }
  
  render() {
    return (
        <View style={{flex: 1}}>
          <Header title="Description" left={this.onHome.bind(this)} icon="home"/>
          <Progress.Bar progress={0.6} color="#61BBB5" width={deviceWidth} />
          <View style={{ paddingTop: 100 }}>
            <Text style={styles.postTitle} onPress={this.removeKeyboard.bind(this)}>Describe It</Text>
            <TextInput 
              placeholder="Enter Description"
              ref="Description" 
              style={ styles.input }
              value={this.props.guide.description} 
              onChangeText={(event) => this.props.dispatch(addPostDescription(event))}/>
            <View style={ styles.line }/>
          </View>
          <View style={ styles.bottomRow } >
            <TouchableOpacity onPress={this.onBack.bind(this)} style={ styles.bottomLeft }>
              <Text style={[styles.white, styles.text]}><Icon style={ styles.white } name="arrow-left" size={25} /> Back</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onNext.bind(this)} style={ styles.bottomRight }>
              <Text style={[styles.white, styles.text]}>Add Places <Icon style={ styles.white } name="arrow-right" size={25} /></Text>
            </TouchableOpacity>
          </View>
        </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    guide: state.guide,
    user: state.user
  };
}

export default connect(mapStateToProps)(PostDescription);