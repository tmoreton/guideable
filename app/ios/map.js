'use strict';
import React, {Component} from 'react';
import firebase from '../config/firebase';
import ReactNative from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = require('../theme/theme')
import Header from '../components/Header';
import { connect } from 'react-redux';
import MapView from 'react-native-maps';
import App from './app';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

const { 
  Text, 
  View, 
  TouchableOpacity,
  Image,
  TextInput,
  AlertIOS,
  Button,
} = ReactNative;

class Map extends Component {

  onHome(){
    this.props.navigator.push({ component: App });
  }

  onBack(){
    this.props.navigator.pop();
  }

  render() {
    return (
      <View style={styles.mapContainer}>
        <MapView
          style={styles.map}
          region={{
            latitude: this.props.guide.city.lat,
            longitude: this.props.guide.city.lng,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        >
          {Object.keys(this.props.guide.places).map((key) => {
            return(
              <MapView.Marker
                coordinate={{
                  latitude: this.props.guide.places[key].lat,
                  longitude: this.props.guide.places[key].lng
                }}
                key={key}
                title={this.props.guide.places[key].name}
                description={this.props.guide.places[key].address}
              />
            )
          })}
        </MapView>
        <TouchableOpacity style={ styles.btn } onPress={this.onBack.bind(this)} >
          <Text style={styles.text}><Icon name="arrow-left" size={25} /> Back</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Map;         

