'use strict';
const styles = require('../theme/theme')
import Header from '../components/Header';
import Post from './postTitle';
import Guide from './guide';
import Profile from './myProfile';
import Icon from 'react-native-vector-icons/FontAwesome';
import firebase from '../config/firebase';
import { signIn } from '../actions'
import { connect } from 'react-redux';
import React, { Component } from 'react';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      guides: []
    }
  }

  componentDidMount(){
    this.getGuides()
  }

  profile(){
    this.props.navigator.push({ component: Profile });
  }

  post(){
    this.props.navigator.push({ component: Post });
  }

  goToPost(){
    this.self.props.navigator.push({ 
      component: Guide,
      passProps: { guide: this.guide } 
    });
  }

  getGuides() {
    var guides = firebase.database().ref('guides');
    guides.once('value', (snap) => {
      var items = [];
      snap.forEach((child) => {
        if(child.val().title != null){
          var item = child.val();
          item.firebaseId = child.key
          items.push(item);  
        }
      });
      items = items.reverse();
      this.setState({ guides: items });
    });
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <Header title="Guideable" left={this.post.bind(this)} right={this.profile.bind(this)} icon="plus-square-o" leftText="Add Guide"/>
        <ScrollView>
          <View style={{ marginTop: -20}}>
          {Object.keys(this.state.guides).map((key) => {
            return (
              <TouchableHighlight >
                <View>
                  <Image source={{uri: this.state.guides[key].image}} style={styles.articlePreview}>
                    <View style={styles.container}/>
                  </Image>
                  <TouchableOpacity style={styles.overlay} key={key} onPress={ this.goToPost.bind({self: this, guide: this.state.guides[key]}) }>
                    <Text style={styles.previewName}>{this.state.guides[key].title}</Text>
                    <Text style={styles.previewCity}><Icon name="globe" size={20} /> {this.state.guides[key].city.name}</Text>
                  </TouchableOpacity>
                </View>
              </TouchableHighlight>
            )
          })}
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    guide: state.guide,
  };
}

export default connect(mapStateToProps)(App);