'use strict';
import React, {Component} from 'react';
import firebase from '../config/firebase';
import ReactNative from 'react-native';
import Profile from './myProfile';
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = require('../theme/theme')
import Header from '../components/Header';
import { connect } from 'react-redux';
import App from './app';
import Map from './map';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

const FBSDK = require('react-native-fbsdk');
const {
  ShareDialog,
} = FBSDK;

const { 
  Text, 
  View, 
  TouchableOpacity,
  Image,
  TextInput,
  AlertIOS,
  ScrollView,
} = ReactNative;

class Guide extends Component {

  onHome(){
    this.props.navigator.push({ component: App });
  }

  profile(){
    this.props.navigator.push({ component: Profile });
  }

  viewMap(){
    this.props.navigator.push({ 
      component: Map,
      passProps: { guide: this.props.guide } 
    });
  }

  share() {
    // const shareLinkContent = {
    //     contentType: 'link',
    //     contentUrl: "https://facebook.com",
    //     contentDescription: 'Wow, check out this great site!',
    // };
    // ShareDialog.canShow(shareLinkContent).then(
    //   function(canShow) {
    //     if (canShow) {
    //       return ShareDialog.show(shareLinkContent);
    //     }
    //   }
    // ).then(
    //   function(result) {
    //     if (result.isCancelled) {
    //       console.log('Share cancelled');
    //     } else {
    //       AlertIOS.alert('Successfuly Posted!');
    //     }
    //   },
    //   function(error) {
    //     AlertIOS.alert('Share fail with error: ' + error);
    //   }
    // );
  }

  save(){
    firebase.database().ref('users/' + this.props.user.id + '/saves/' + this.props.guide.firebaseId).set(1)
    firebase.database().ref('guides/' + this.props.guide.firebaseId + '/saves/' + this.props.guide.firebaseId).set(1)
    AlertIOS.alert("Added to your Favorites")
  }

  render() {
    return (
        <View style={{flex: 1}}>
          <Header title="Guide" left={this.onHome.bind(this)} right={this.profile.bind(this)} icon="home"/>
          <View style={{flexDirection:'row', marginBottom: -10, alignSelf: 'center'}}>
            <Text style={styles.postDescription}>Created By:{"\n"}{this.props.guide.user.name}</Text>
            <Image source={{uri: this.props.guide.user.photo}} style={{width: 30, height: 30, borderRadius: 15 }}/>
          </View>
          <ScrollView >
            <Image source={{uri: this.props.guide.image}}  style={{  width: deviceWidth, height: (deviceWidth*.5), marginTop: -20 }}/>
            <Text style={styles.postTitle}>{this.props.guide.title}</Text>
            <Text style={styles.postCity}><Icon name="globe" size={20} /> {this.props.guide.city.name}</Text>
            <Text style={styles.postDescription}>{this.props.guide.description}</Text>
            <View style={ styles.line }/>
            {Object.keys(this.props.guide.places).map((key) => {
              if(this.props.guide.places[key] != '' && this.props.guide.places[key].name != ''){
                var icon;
                switch(this.props.guide.places[key].category) {
                  case "cafe":
                    icon = "coffee"
                    break;
                  case "bakery":
                    icon = "coffee"
                    break;
                  case "restaurant":
                    icon = "cutlery"
                    break;
                  case "bar":
                    icon = "glass"
                    break;
                  default:
                    icon = "map-marker"
                }
                return (
                  <TouchableOpacity onPress={this.viewMap.bind(this)} style={{ padding: 5, paddingBottom: 0}} >
                    <Text style={styles.text}><Icon name={icon} size={18} /> {this.props.guide.places[key].name}</Text>
                    <Text style={styles.postDescription}>{this.props.guide.places[key].address.split(",")[0]}</Text>
                  </TouchableOpacity>
                )
              }
            })}
            <View style={ styles.line }/>
            <TouchableOpacity style={ styles.btn } onPress={this.viewMap.bind(this)} >
              <Text style={styles.text}><Icon name="map" size={20} /> View Map</Text>
            </TouchableOpacity>
            <TouchableOpacity style={ styles.btn } onPress={this.save.bind({self: this, guide: this.props.guide})} >
              <Text style={styles.text}> Save Guide</Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

export default connect(mapStateToProps)(Guide);