'use strict';
import React, {Component} from 'react';
import ReactNative from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = require('../theme/theme')
import Header from '../components/Header';
import { addPostCity, updatePostCity } from '../actions'
import { connect } from 'react-redux';
import App from './app';
import Description from './postDescription';
import * as Progress from 'react-native-progress';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;

const { 
  Text, 
  View, 
  TouchableOpacity,
  Image,
  TextInput,
  Keyboard,
  ActivityIndicator,
  AlertIOS
} = ReactNative;

class PostDescription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: '',
      animating: false
    }
  }

  onHome(){
    this.props.navigator.push({ component: App });
  }

  onBack(){
    this.props.navigator.pop();
  }

  onNext(){
    if(this.props.guide.city.name != ''){
      this.props.navigator.push({ component: Description });
    } else {
      AlertIOS.alert("Search City")
    }
  }

  addCity(){
    Keyboard.dismiss()
    if( this.state.city != '' ){
      this.setState({animating: true})
      this.props.dispatch(addPostCity(this.state.city))
      this.setState({city: ''})
    } else {
      AlertIOS.alert("Add City")
    }
  }

  removeKeyboard(){
    Keyboard.dismiss()
  }

  renderCity(){
    if(this.props.guide.city.name != ''){
      return (
        <View>
          <Text style={styles.cityText}><Icon name="globe" size={25} />  { this.props.guide.city.name } <Icon name="check" size={20} color="#61BBB5" /></Text>
        </View>
      )
    } else {
      return (
        <View>
          <ActivityIndicator animating={this.state.animating} size = "large" />
        </View>
      )
    }
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <Header title="City" left={this.onHome.bind(this)} icon="home"/>
        <Progress.Bar progress={0.4} color="#61BBB5" width={deviceWidth} />
        <View style={{ paddingTop: 100 }}>
          <Text style={styles.postTitle} onPress={this.removeKeyboard.bind(this)}>What City?</Text>
          <TextInput 
            placeholder="Enter City"
            ref="City" 
            style={ styles.input }
            value={ this.state.city }
            onChangeText={(event) => this.setState({city: event})}/>
          <View style={ styles.line }/>
          <TouchableOpacity onPress={this.addCity.bind(this)} style={styles.postBtn}>
            <Text style={[styles.text, styles.white]}>Search <Icon name="search" size={15} /></Text>
          </TouchableOpacity>
          {this.renderCity()}
        </View>
        <View style={ styles.bottomRow } >
          <TouchableOpacity onPress={this.onBack.bind(this)} style={ styles.bottomLeft }>
            <Text style={[styles.white, styles.text]}><Icon style={styles.white} name="arrow-left" size={25} /> Back</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.onNext.bind(this)} style={ styles.bottomRight }>
            <Text style={[styles.white, styles.text]}>Add Description <Icon style={styles.white} name="arrow-right" size={25} /></Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    guide: state.guide,
    user: state.user
  };
}

export default connect(mapStateToProps)(PostDescription);