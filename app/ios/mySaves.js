'use strict';
const styles = require('../theme/theme')
import Header from '../components/Header';
import Post from './postTitle';
import Guide from './guide';
import Profile from './myProfile';
import App from './app';
import Icon from 'react-native-vector-icons/FontAwesome';
import firebase from '../config/firebase';
import { connect } from 'react-redux';
import React, { Component } from 'react';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';

class MySaves extends Component {
  constructor(props) {
    super(props);
    this.state = {
      guides: []
    }
  }

  componentDidMount(){
    this.getKeys()
  }

  profile(){
    this.props.navigator.push({ component: Profile });
  }

  back(){
    this.props.navigator.pop();
  }

  home(){
    this.props.navigator.push({ component: App });
  }

  goToPost(){
    this.self.props.navigator.push({ 
      component: Guide,
      passProps: { guide: this.guide } 
    });
  }

  getKeys() {
    var userSaves = firebase.database().ref('users/' + this.props.user.id + '/saves');
    userSaves.once("value", (snap) => {
      var keys = [];
      snap.forEach((child) => {
        keys.push(child.key);    
      });
      this.getGuides(keys);
    });
  }

  getGuides(keys){
    var items = []
    for(var i = 0; i < keys.length; i++ ){
      var guides = firebase.database().ref('guides/' + keys[i]);
      guides.once("value", (snap) => {
        if(snap.val() != null){
          items.push(snap.val())
          this.setState({ guides: items });
        }
      });
    }
  }

  render() {
    if ( this.state.guides.length < 1 ){
      return(
        <View style={{flex: 1}}>
          <Header title="My Guides" left={this.back.bind(this)} icon="arrow-left"/>
          <Text style={ styles.text}>You Haven't Saved A Guide Yet :(</Text>
          <TouchableOpacity style={ styles.postBtn } onPress={ this.home.bind(this) } >
            <Text style={[ styles.text, styles.white ]}>Add One!</Text>
          </TouchableOpacity>
        </View>
      )
    } else {
      return (
        <View style={{flex: 1, }}>
          <Header title="Saved Guides" left={this.back.bind(this)} icon="arrow-left"/>
          <ScrollView>
            <View>
            {Object.keys(this.state.guides).map((key) => {
              return (
                <TouchableHighlight >
                  <View>
                    <Image source={{uri: this.state.guides[key].image}} style={styles.articlePreview}>
                      <View style={styles.container}/>
                    </Image>
                    <TouchableOpacity style={styles.overlay} key={key} onPress={ this.goToPost.bind({self: this, guide: this.state.guides[key]}) }>
                      <Text style={styles.previewName}>{this.state.guides[key].title}</Text>
                      <Text style={styles.previewCity}><Icon name="globe" size={20} /> {this.state.guides[key].city.name}</Text>
                    </TouchableOpacity>
                  </View>
                </TouchableHighlight>
              )
            })}
            </View>
          </ScrollView>
        </View>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    guide: state.guide,
    user: state.user,
  };
}

export default connect(mapStateToProps)(MySaves);