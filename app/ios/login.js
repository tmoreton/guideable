'use strict';

import styles from '../theme/theme';
import App from './app';
import Post from './postTitle';
import Terms from './terms';
import firebase from '../config/firebase';
import { login } from '../actions';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import React, { Component } from 'react';
const FBSDK = require('react-native-fbsdk');

const {
  LoginButton,
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} = FBSDK;

import {
  View,
  Text,
  TouchableOpacity,
  InteractionManager,
} from 'react-native';

class Login extends Component {

  fbApi(){
    AccessToken.getCurrentAccessToken().then(
      (data) => {
        let accessToken = data.accessToken
        fetch('https://graph.facebook.com/v2.5/me?access_token=' + accessToken)
          .then((response) => response.json())
          .then((json) => {
            this.props.dispatch(login(json))
            this.props.navigator.push({ component: App });
            firebase.database().ref('users/' + json.id + '/profile').set(json)
          })
          .catch(() => {
            reject('ERROR GETTING DATA FROM FACEBOOK')
          })
        new GraphRequestManager().addRequest(infoRequest).start()
      }
    )
  }

  firstLogin(){
    AccessToken.getCurrentAccessToken().then(
      (data) => {
        let accessToken = data.accessToken
        fetch('https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=' + accessToken)
          .then((response) => response.json())
          .then((json) => {
            this.props.dispatch(login(json))
            this.props.navigator.push({ component: App });
            firebase.database().ref('users/' + json.id + '/profile').set(json)
            this.mailchimp(json);
          })
          .catch(() => {
            reject('ERROR GETTING DATA FROM FACEBOOK')
          })
        new GraphRequestManager().addRequest(infoRequest).start()
      }
    )
  }

  terms(){
    this.props.navigator.push({ component: Terms });
  }

  mailchimp(json){
    // fetch('https://us3.api.mailchimp.com/3.0/lists/d466474ae3/members?apikey=b487fbc172331f18d7e8e9386abd055e&id=d466474ae3&email[email]=tmoreton89@gmail.com&merge_vars[FNAME]=John&merge_vars[LNAME]=Doe&double_optin=true&send_welcome=false').then(function(response){
    //   console.log(response)
    //   })
    let authenticationString = btoa('randomstring:b487fbc172331f18d7e8e9386abd055e');
    authenticationString = "Basic " + authenticationString;
    fetch('https://us3.api.mailchimp.com/3.0/lists/d466474ae3/members', {
      method: 'POST',
      mode: 'no-cors',
      headers: {
        'authorization': authenticationString,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email_address: json.email, 
        status: "subscribed"
      })
    }).then(function(e){
        console.log(e)
    }).catch(function(e){
        console.log("fetch error");
    })

  }

  componentWillMount() {
    InteractionManager.runAfterInteractions(() => {
      this.fbApi();
    });
  }

  render() {
    return (
      <View style={[styles.loginScreen]}>
        <Icon style={styles.white} name="map" size={60} />
        <Text style={styles.logoText}>Guideable</Text>
        <LoginButton
          publishPermissions={["publish_actions"]}
          onLoginFinished={
            (error, result) => {
              if (error) {
                alert("login has error: " + result.error);
              } else if (result.isCancelled) {
                alert("login is cancelled.");
              } else {
                this.firstLogin();
              }
            }
          }/>
        <TouchableOpacity onPress={this.terms.bind(this)}>
          <Text style={styles.white}>Terms & Conditions</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    userName: state.userName,
    userPhoto: state.userPhoto,
    userId: state.userId,
  };
}

export default connect(mapStateToProps)(Login);
