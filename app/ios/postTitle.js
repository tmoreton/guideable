'use strict';
import React, {Component} from 'react';
import ReactNative from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = require('../theme/theme')
import Header from '../components/Header';
import { addPostTitle } from '../actions'
import { connect } from 'react-redux';
import City from './postCity';
import App from './app';
import * as Progress from 'react-native-progress';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;

const { 
  Text, 
  View, 
  TouchableOpacity,
  Image,
  TextInput,
  Keyboard,
  AlertIOS,
} = ReactNative;

class PostTitle extends Component {

  onHome(){
    this.props.navigator.push({ component: App });
  }

  onNext(){
    if(this.props.guide.title != ''){
      this.props.navigator.push({ component: City });
    } else if (this.props.guide.title == ''){
      AlertIOS.alert("Name Your Guide")
    }
  }

  removeKeyboard(){
    Keyboard.dismiss()
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <Header title="Title" left={this.onHome.bind(this)} icon="home"/>
        <Progress.Bar progress={0.2} color="#61BBB5" width={deviceWidth} />
        <View style={{ paddingTop: 100 }}>
          <Text style={styles.postTitle} onPress={this.removeKeyboard.bind(this)}>Name Your Guide</Text>
          <TextInput 
            placeholder="Enter Name"
            ref="Title" 
            maxLength = {30}
            style={ styles.input }
            value={this.props.guide.title} 
            onChangeText={(event) => this.props.dispatch(addPostTitle(event))}/>
          <View style={ styles.line }/>
        </View>
        <View style={ styles.bottomRow } >
          <View style={ styles.bottomBlock }></View>
          <TouchableOpacity onPress={this.onNext.bind(this)} style={ styles.bottomRight }>
            <Text style={[styles.white, styles.text]}>Add City <Icon style={styles.white} name="arrow-right" size={25} /></Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    guide: state.guide,
    user: state.user
  };
}

export default connect(mapStateToProps)(PostTitle);