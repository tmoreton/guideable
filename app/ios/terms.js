'use strict';
const styles = require('../theme/theme')
import Header from '../components/Header';
import Login from './login';
import React, { Component } from 'react';
var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

import {
  View,
  Text,
  ScrollView,
} from 'react-native';

class Terms extends Component {

  back(){
    this.props.navigator.push({ component: Login });
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <Header title="Terms & Conditions" left={this.back.bind(this)} icon="arrow-left"/>

        <ScrollView>
          <Text style={{fontSize: 22, textAlign: 'center',  alignSelf: 'center'}}>End-User License Agreement</Text>
          <Text style={{textAlign: 'center',  alignSelf: 'center'}} >
            Last updated: 6/26/2017
            {'\n'}
            Please read this End-User License Agreement ("Agreement") carefully before using My Guideable ("Application").
            {'\n'}
            By using the Application, you are agreeing to be bound by the terms and conditions of this Agreement.
            {'\n'}
            If you do not agree to the terms of this Agreement, do not use the Application.
            {'\n'}
            By signing into the Application you agree that Guideable is allowed to add your email address to our email list to reach out in the future for new offers and/or feedback from using the Application.
            {'\n'}
          </Text>
          <Text style={{fontSize: 22, textAlign: 'center',  alignSelf: 'center'}}>License</Text>
          <Text style={{textAlign: 'center',  alignSelf: 'center'}} >
            Guideable  grants you a revocable, non-exclusive, non-transferable, limited license to download, install and use the Application solely for your personal, non-commercial purposes strictly in accordance with the terms of this Agreement.
            {'\n'}
         </Text>
          <Text style={{fontSize: 22, textAlign: 'center',  alignSelf: 'center'}}>Restrictions</Text>
          <Text style={{textAlign: 'center',  alignSelf: 'center'}} >
            You agree not to, and you will not permit others to:
            {'\n'}
            a) license, sell, rent, lease, assign, distribute, transmit, host, outsource, disclose or otherwise commercially exploit the Application or make the Application available to any third party.
            {'\n'}
          </Text>
          <Text style={{fontSize: 22, textAlign: 'center',  alignSelf: 'center'}}>Modifications to Application</Text>
          <Text style={{textAlign: 'center',  alignSelf: 'center'}} >
            Guideable reserves the right to modify, suspend or discontinue, temporarily or permanently, the Application or any service to which it connects, with or without notice and without liability to you.
            {'\n'}
          </Text>
          <Text style={{fontSize: 22, textAlign: 'center',  alignSelf: 'center'}}>Term and Termination</Text>
          <Text style={{textAlign: 'center',  alignSelf: 'center'}} >
            This Agreement shall remain in effect until terminated by you or Guideable
            {'\n'}
            Guideable may, in its sole discretion, at any time and for any or no reason, suspend or terminate this Agreement with or without prior notice.
            {'\n'}
            This Agreement will terminate immediately, without prior notice from Guideable, in the event that you fail to comply with any provision of this Agreement. You may also terminate this Agreement by deleting the Application and all copies thereof from your mobile device or from your desktop.
            {'\n'}
            Upon termination of this Agreement, you shall cease all use of the Application and delete all copies of the Application from your mobile device or from your desktop.
            {'\n'}
          </Text>
          <Text style={{fontSize: 22, textAlign: 'center',  alignSelf: 'center'}}>Severability</Text>
          <Text style={{textAlign: 'center',  alignSelf: 'center'}} >
            If any provision of this Agreement is held to be unenforceable or invalid, such provision will be changed and interpreted to accomplish the objectives of such provision to the greatest extent possible under applicable law and the remaining provisions will continue in full force and effect.
            {'\n'}
          </Text>
          <Text style={{fontSize: 22, textAlign: 'center',  alignSelf: 'center'}}>Amendments to this Agreement</Text>
          <Text style={{textAlign: 'center',  alignSelf: 'center'}} >
            Guideable reserves the right, at its sole discretion, to modify or replace this Agreement at any time. If a revision is material we will provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.
            {'\n'}
          </Text>
        </ScrollView>

      </View>
    );
  }
}

export default Terms;