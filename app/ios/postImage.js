'use strict';
import React, {Component} from 'react';
import ReactNative from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = require('../theme/theme')
import Header from '../components/Header';
import { addPostImage } from '../actions'
import { connect } from 'react-redux';
import App from './app';
import Review from './postReview';

var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;
const { 
  Text, 
  View, 
  TouchableOpacity,
  TextInput,
  Image,
  AlertIOS,
  ActivityIndicator,
} = ReactNative;

class PostImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false
    }
  }

  onHome(){
    this.props.navigator.push({ component: App });
  }

  onNext(){
    if(this.props.guide.image != ''){
      this.props.navigator.push({ component: Review });
    } else {
      AlertIOS.alert("Add Image")
    }
  }

  onBack(){
    this.props.navigator.pop();
  }

  addImage(){
    this.props.dispatch(addPostImage())
    this.setState({ animating: true })
  }

  renderImage(){
    if(this.props.guide.image != ''){
      return (
        <View>
          <Image source={{uri: this.props.guide.image}}  style={{width: deviceWidth, height: (deviceWidth*.5)}}/>
        </View>
      )
    } else {
      return (
        <View>
          <ActivityIndicator animating={this.state.animating} size = "large" />
        </View>
      )
    }
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <Header title="Photo" left={this.onHome.bind(this)} icon="home"/>
        <View>
          { this.renderImage() }
          <Text style={styles.postTitle}>Add Photo</Text>
          <TouchableOpacity style={{ alignItems: 'center'}} onPress={this.addImage.bind(this)} >
            <Icon name="camera" size={50} />
          </TouchableOpacity >
        </View>
        <View style={ styles.bottomRow } >
          <TouchableOpacity onPress={this.onBack.bind(this)} style={ styles.bottomLeft }>
            <Text style={[styles.white, styles.text]}><Icon style={ styles.white } name="arrow-left" size={25} /> Back</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.onNext.bind(this)} style={ styles.bottomRight }>
            <Text style={[styles.white, styles.text]}>Review <Icon style={ styles.white } name="arrow-right" size={25} /></Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    guide: state.guide,
    user: state.user
  };
}

export default connect(mapStateToProps)(PostImage);